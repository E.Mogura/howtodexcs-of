\chapter{DEXCS for OpenFOAM（DEXCS-OF）とは}
\label{sec:aboutDEXCS-OF}

\begin{quote}
本書の想定読者は、主としてDEXCS-OFを既に使った事のあるユーザーであるが、そうでない本当の初体験者の人にも読んでもらえるよう構成したのが本章である。また本章の内容は、過去(2018/3)に日本設計工学会の会誌に投稿した解説記事
\footnote{
\label{kaisetu}
日本設計工学会会誌『設計工学』 Vol.53,No.3 2018 http://www.jsde.or.jp/shuppan-j/2018/jl201803.html
}
の内容をほとんどそのまま焼き直したもので、すでにご存知の方は読み飛ばしてもらっても構わない。
\end{quote}

\section{OpenFOAMとは}

　OpenFOAM®は、その前身の商用コードFOAM（Field Operation And Manipulationの略）が、2004年12月OpenCFD社
\footnote{
\label{OpenCFD}
ESI-OpenCFD https://www.openfoam.com/about/
}
 によってオープンソース化されたものであり、C++で記述された流体計算用ライブラリを使用して、当初はその名前の示す通り「場の操作」すなわち偏微分方程式で記述された場の方程式を容易にプログラム化できる事を謳い文句にしたソフトウェアであった。

たとえば速度場（U）におけるスカラー（T）の輸送方程式

\begin{center}


\[
\frac{\partial}{\partial t} T+ \bigtriangledown  \bullet (  U T)- \bigtriangledown  \bullet (Dt \bigtriangledown T)=St
\]
Dt：拡散係数 

St：ソース項
\end{center}

を記述するのに、
\begin{quotation}
\begin{tcolorbox}
\begin{verbatim}
solve
(
fvm::ddt(T)
+fvm::div(phi, T)
−fvm::laplacian(Dt, T)
== fvOption(T)
);
\end{verbatim}
\end{tcolorbox}
\end{quotation}

と簡単に書ける。ここで、ddtは偏微分記号の$\frac{\partial}{\partial t}$、divは$\bigtriangledown  \bullet $、laplacianは$\bigtriangledown  \bullet \bigtriangledown$、fvm::は陰解法、fvOptionはソース項計算を表しており、これらを計算するライブラリが充実し、オブジェクト化されているので、自分が解きたい問題に合わせて新規プログラム開発が短時間で実現可能になるという特長があった。

リリース当初は大学や研究機関を中心にCFD開発ツールとして利用されることが多かった。その後かようにして作成されたソルバーが標準ソルバーとなっていく。標準チュートリアルケース（たとえば図1）と併せて同梱されているものとしては、

\begin{itemize}

\item 非圧縮性流体の定常/非定常乱流解析
\item 圧縮性流体の定常/非定常熱対流解析
\item 流体・固体伝熱 (CHT) 解析
\item 混相流解析 (界面追跡法/多流体モデル)
\end{itemize}

など、商用のCFDソフトに比べて遜色ないレベルになってきており、近年では企業内ユーザーが、同梱されている標準ソルバーをそのまま実用計算に利用する事例も増加しつつある。
\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{dexcs/1-1.png}
\caption{OpenFOAM標準チュートリアルの一例} 
\label{fig:tutorialExample}
\end{figure}

一方、企業内ユーザーが増えてきた理由には、もう一つsnappyHexMeshという独自のメッシュ生成ツールが同梱されるようになった事が大きい。すなわち、実際の製品CADモデルを対象にしたメッシュ作成がOpenFOAMだけで実現できるようになったということである。

そもそもOpenFOAMは有限体積法で定式化されており、メッシュは空間を任意の多面体（面の数はいくらでも良い）で隙間なく分割し、これをpolyMeshと呼ばれる所定の書式で所定の場所に収納しておけば良いという体系になっている。

しかるに、リリース当初は、blockMeshと呼ばれる六面体構造格子を自動作成するツールは存在していたが、複雑形状問題に適用することは事実上出来なかった。複雑形状の解析には，他の専用メッシュ生成ソフトウェアを用いてメッシュを作成し，これを OpenFOAM 環境に読み込んで使用する。その際に使用するフォーマット変換ツールが多数用意されている。現在でもかような使い方をしているユーザーは多く存在する。

snappyHexMeshは、2008年のOpenFOAM-1.5よりリリース、STLに代表される形状の表面ポリゴンデータに適合した六面体ベースの自動メッシュ作成ツールである。複雑形状モデルの解析をしたいユーザーは一般的な3次元CADツールで作成したデータをSTL形式に変換し、STLのパーツ毎にメッシュ細分化レベルなどを所定の書式にて指定したsanappyHexMeshDictという設定ファイルを用意しておく。上述のblockMeshにて解析対象領域を包含するベースとなるメッシュを作成した後は全自動でメッシュを作成してくれるようになった。
\begin{figure}[H]
\centering
\includegraphics[scale=0.65]{dexcs/1-2.png}
\caption{snappyHexMesh作成手順と主要パラメタの概要} 
\label{fig:snappyHexMeshSummary}
\end{figure}

その際、特に複雑なCADデータをSTLデータに変換する際に、一般的にSTLデータに穴が開くとか、二重節点が生じるなど、ダーティジオメトリ問題が生じることが多いが、指定したメッシュサイズの最小サイズ以下のエラーであればメッシュ作成に支障ないというロバスト性もあって、実用面で受け入れられるようになってきている。

一方、2014年OpenFOAM-2.3.0より、新たにfoamyHexMeshという自動メッシュ作成ツールもリリースされた。snappyHexMeshでは細分化の方法は八分木法に依るもので、細分化レベルが異なる領域間のメッシュ粗度が急激に変化するのに対し、六面体サイズが徐変してくれる（図\ref{fig:formyHexVSsnappyHex}）。

\begin{figure}[H]
\centering
\includegraphics[scale=0.75]{dexcs/1-3.png}
\caption{foamyHexMeshとsnappyHexMeshを比較} 
\label{fig:formyHexVSsnappyHex}
\end{figure}

しかしながら、現状では形状データに対する完全性が要求されることや、メッシュ品質の問題、計算時間が長い等の問題がある。多くのユーザーが改良を期待しているが、現在のところ開発は中断しているようである。

以上、簡単にOpenFOAMの進化を振り返ってきた。進化の過程で、OpenCFD社は、2011年8月にはSGI社、さらに2012年9月にESIに買収されたが、OpenFOAMは非営利団体OpenFOAM Foundation2)から引き続きオープンソースとして公開されるようになり、現在はOpenFOAMといえば、OpenFOAM Foundation による OpenFOAM と、OpenCFD 社 (ESI) による OpenFOAM+（2017年6月よりOpenFOAM-vYYMMと命名）という形で並び立っている。

また、これら以外にも多くのFork、派生バージョンが生まれ現在に至っている（図\ref{fig:ofrevisions}はFoam-Uのサイト
\footnote{
\label{Foam-U}
Foam-Uのサイト http://foam-u.fr/openfoam-quelle-distribution-et-par-qui/
}
より引用したもの）。
\begin{figure}[H]
\centering
\includegraphics[scale=0.32]{dexcs/1-4.png}
\caption{OpenFOAMの様々な派生バージョン} 
\label{fig:ofrevisions}
\end{figure}
　
\section{DEXCSとは}

DEXCSは、Digital Engineering on eXtensible Computing Systemの頭文字から名付けられ、拡張性を持つ設計支援用解析システムとして、以下の３つの目標を持って開発されているオープンCAEシステムである。

\begin{itemize}

\item 構築や運用に手間がいらず，手軽に利用できる 
\item 計算だけでなく，プリポスト機能まで対応する
\item モジュールを自由に追加できる 
\end{itemize}

これらの目標の下、DEXCSは以下の特徴を持つシステムとなっている。

\begin{itemize}

\item 基本OS（Linux）の上にCAEに必要なツールを全て組み込んであり、All-In-Oneで配布できる。
\item ダウンロードしたISOイメージを用いて、DVD起動や仮想マシン構築が可能で、HDDにインストールも出来る。
\item 操作はDEXCSランチャーに集約され、最小限の簡単なマウス操作でCAE解析を効率的に実現する。 
\item 初心者のCAE習得を支援するために、サンプル形状や初期設定が用意されており、確実に解析演習できる。
\end{itemize}

開発当初は、株式会社デンソーでの社内CAE教育の教材として使う事をターゲットに岐阜工業高等専門学校との間で共同研究されたものであったが、現在では共同研究は終了してDEXCS公式サイトより誰でも無償でダウンロードすることができるようになっている。

また、図\ref{fig:dexcs-history}に示すよう、開発当初の線形構造解析（Adventure）版は開発が完了しているが、その他の非線形構造解析版や流体解析版は、ほぼ毎年更新版がリリースされている。
\begin{figure}[H]
\centering
\includegraphics[scale=0.45]{dexcs/1-5.png}
\caption{DEXCS公開版の経緯} 
\label{fig:dexcs-history}
\end{figure}

当初からCAE教育がターゲットであったとはいえ、教育で使った後の実用性も考慮して3次元の複雑形状が取り扱えることは大前提であった。そうなると、オープンCAEの分野では、単独のソフトで実務に使えそうなものはほとんど存在しなかった。

図\ref{fig:dexcs-adventure}は、DEXCSとして最初にリリースしたAdventure版のシステム構成図で、Adventure
\footnote{
\label{adventure}
adventureのサイト https://adventure.sys.t.u-tokyo.ac.jp/jp/
}
そのものにもプリ・ポスト機能は存在していたが、より高機能で使いやすいオープン系ツールと組み合わせて使った方がユーザー本位であるという考え方でパッケージ化し、簡易ランチャーからソルバーやプリ・ポストソフトを起動したり、パラメタ編集できるようにした。
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{dexcs/1-6.png}
\caption{DEXCS-Adventure版のシステム構成図} 
\label{fig:dexcs-adventure}
\end{figure}

\section{OpenFOAMの実践的活用方法}
\label{sec:1-3}
OpenFOAMを使いたい人の目的は大きく2つに大別される。すなわち、

\begin{itemize}
\item OpenFOAMをカスタマイズして独自のソルバーを作りたい
\item OpenFOAMの標準ソルバー、あるいは他者が作ったカスタムソルバーを使いたい
\end{itemize}

もちろん、どちらもやりたいという人もおられようが、DEXCS-OFは後者の観点で、「{\textbf  誰にでも簡単、すぐに、OpenFOAMを使える} 」を狙いに開発・更新を続けてきた。

ここでは、後者の観点で、もう少し具体的にOpenFOAMを使うとはどういうことなのかについて言及しておきたい。

先にOpenFOAMでは、非圧縮性流体の定常/非定常乱流解析を始めとして、様々な解析を実施できると記したが、解析の種類が異なれば、使用するソルバーが変わって、必要なパラメタセットの内容も異なったものになり、これらがメッシュデータや計算結果も含めてひとつのフォルダ中に所定の形式で収納されている。OpenFOAMでは、これをケースファイルと呼んでいる。

様々なソルバーがあると記したが、具体的なソルバーの数は200もある。それぞれのソルバーでパラメタセットが異なるからと云って、その使い方説明書が存在する訳ではない（代表的なものは存在するが・・・）。

ただ、OpenFOAMには標準チュートリアルといって、ほとんどのソルバーに対して、雛形ケースファイル（全350）が用意されており、所定の簡単なコマンドでメッシュ作成から計算実行まで実施できるようになっている。

したがって、この標準チュートリアルケース、あるいは他者が作ったソルバーであっても、それが動作する具体的なケースファイルが存在すれば、パラメタセットの意味を調べて（判らない場合は具体的に値を変更して結果から逆推定したりすることもある）、その値を自分がやりたい問題向けに適合して使えば良いことになる。

問題は境界条件である。ソルバーが異なれば、求解したいField変数が変わるし、境界条件の種別も各Field変数に応じて様々なタイプが存在し、詳しくは後述するが、かなり面倒な作業になる。

なおメッシュファイル自体は基本的にソルバーが異なっても使い回しの出来るファイル構造になっているので、OpenFOAMの標準ツール以外にも様々なメッシュ作成法が適用可能である（詳しくは後述）。

したがって、OpenFOAMの一般的なユーザーは、

\begin{enumerate}
\item 他の解析例や標準チュートリアルケースを精査し、自分が解きたい現象・モデルに近いものを探し出す。
\item 上記ケースファイルのメッシュを自前で作成したメッシュに置き換える。
\item モデルパラメタ、境界条件の細部詳細を整合する。
\end{enumerate}
という手順で使っている。

これが商用ソフトの場合と根本的に違うところであり、一般的な商用ソフトであれば、第1項はあまり意識する必要はなく、上記3項をメニュー等で選択できるのが普通である。OpenFOAMでは、これ（第3項）をテキストファイルベースで直接編集・変更する必要があるのと、第1項の選択次第で、変更対象が全く異なったものになってしまうという点である。

かような事情を踏まえれば、DEXCS-OFは「{\textbf 誰にでも簡単、すぐに、仮想風洞試験（simpleFoam）を使える}」というのが実体で、その他のソルバーを使うに際しては、上述の困難が伴う。この困難さの程度をいかに少なくしてきたか？がDEXCS-OFの更新の歴史でもある。

\section{DEXCS-OFの歴史}

DEXCSのOpenFOAM版は、DEXCS2008 for OpenFOAM として、2008年よりリリースを開始した。

OpenFOAMも当時はJAVAで開発されたFoamXというツールが添付され、条件設定やソルバーの起動をGUIから行うことができていた。

したがって、DEXCS2008 for OpenFOAM では、それまで構造解析用DEXCSで培ったプリ処理（blenderでモデル作成、Adventureのtetmeshでメッシュ作成）をFoamXに繋げるだけのものであった。

\begin{figure}[H]
\centering
\includegraphics[scale=0.45]{dexcs/1-7.png}
\caption{FoamXの操作画面例} 
\label{fig:FoamX}
\end{figure}

話は脱線するが、図\ref{fig:FoamX}のキャプチャーイメージは、本稿作成に際し、公開したDEXCS2008 for OpenFOAMを使って、改めて撮り直したものである。FoamXのソースコードは現在でも入手可能ではあるが、現在のOS環境でビルドすることは大変困難（ほとんど不可能？）になっており、当時のOS環境が現在でもそのまま使えるという仮想環境イメージで配布した事は、過去の研究・解析結果を確実に再現できるという意味においても良かったと考えている。

しかしながらこのFoamXはOpenFOAM-1.5以降開発が停止され、現在ではパッケージに含まれていない。

そこで、DEXCS2009からは、DEXCSの原点に立ち帰り、FoamXに代わるオープン系GUIツールで周辺機能を補完し、実用性を考慮し3次元複雑形状モデルを対象とした仮想風洞試験を模したDEXCSランチャー（図\ref{fig:DEXCS-launcher}）を併せ搭載し現在に至っている。

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{dexcs/1-8.png}
\caption{DEXCSランチャーの嬉しさ} 
\label{fig:DEXCS-launcher}
\end{figure}

これは特にLinuxもほとんど知らないOpenFOAMの初心者向けに、ボタンを順番に押していくだけで、一通りの解析が出来るようになっており、動画チュートリアルを通して、まさに触りながら勉強してもらう事を狙っている。

DEXCS2009以来、現在の最新版であるDEXCS2020に至るまで、対象例題と基本的な手順は変えていない。しかし内部仕様としてメッシュ作成ツールや、その設定パラメタを作成するツールをその都度最新のものに対応させるとか、多言語（現時点では英語版のみ）対応できるようになったなどの進化は続けている。

但し、このDEXCSランチャーで出来る事は、あくまで仮想風洞試験だけであり、

\begin{itemize}
\item 形状変更
\item メッシュ法案
\item 風速条件
\end{itemize}
など任意に変更できるというにすぎない。かような機能だけであっても、それ（仮想風洞試験）だけが出来れば良いという人には、ものづくりに役立ったという例7)も報告されるようになったが、ほとんどのユーザーにとって、これだけでは不十分であろう。やはり汎用的なGUIは必要だ。

商用の汎用CFDツールでは一般的に、

\begin{itemize}
\item 定常か非定常か
\item 圧縮性か非圧縮か
\item 温度場の計算有無
\item 乱流モデル
\item 動的メッシュ計算の有無
\item 単領域か複領域か
\end{itemize}
といった項目を選択すれば、応じたパラメタセットが現れるのでその中から選択するという使い方になっている。つまり計算に本来必要なパラメタファイルをユーザーには見せないようにしている。

オープン系のGUIツールで同様のコンセプトで使用可能なソフトも存在する。たとえば、HELYX-OS
\footnote{
\label{HELYX-OS}
HELYX-OS https://engys.com/products/helyx-os
}
という商用ソフトHELYX9)のオープンソース版があり、DEXCS for OpenFOAM にも同梱されている。すぐに使えるようになっているので、商用のCFDツールを使った事がある人には、こちらから入った方が馴染みやすいかもしれない。

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{dexcs/1-9.png}
\caption{HELYX-OSの操作画面例} 
\label{fig:HELYX-OS}
\end{figure}

しかし、商用版に比べると使えるソルバー種類が少なく、OpenFOAMの一部のソルバーにしか対応できていないというか、機能制限されている。

また、メニューが多岐・多階層にわたり、どこから手をつけたらわからないなど、どうしても初心者には敷居が高い。

そこでDEXCSではGUIツール対する要求機能として、商用ツールのそれとは異なる考え方を基本としている。

つまり、初心者には、ボタン等の数を極力減らして使えるようにする。とはいうもの、これをブラックボックス的に使うのではなく、OpenFOAMの仕組み（ファイルやコマンド体系）も同時に少しずつ勉強出来る仕組みも併せて使えるようにしておいて、次の段階（中級者向け）に進んでもらう（図\ref{fig:DEXCS-firstTime}）。

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{dexcs/1-10.png}
\caption{DEXCS for OpenFOAMの初心者向け推奨利用法} 
\label{fig:DEXCS-firstTime}
\end{figure}

OpenFOAMの仕組みがある程度理解できた人であれば、GUIとして、商用ソフトのように、一般的表現を介してブラックスボックス化せずとも、OpenFOAMのパラメタファイル編集やコマンド発行を直接的に省力化してくれる簡易GUIツールがあれば良いという考え方である。

かような考え方に基づいて作成されたOpenFOAMの中級者向けGUIツールがTreeFoamである。一見したところ一般的なファイルマネージャと外観は似通っており同様な使い方も可能であるが、随所でOpenFOAMのパラメタ設定を簡易化するツールを起動できるようになっている。いわばOpenFOAMのケースファイルマネージャといってよいだろう。

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.55]{dexcs/1-11.png}
\caption{TreeFoamの操作画面例} 
\label{fig:TreeFoam}
\end{figure}

CAEの分野でも近年ではクラウド利用が進んでおり、ライセンスフリーであるOpenFOAMが搭載されたクラウドスパコンも提供されるようになってきた。TreeFoam最新版では、リモートサーバーでの作業も省力化できるべく対応している。

その他、OpenFOAMの本体以外で同梱しているツールについても一部紹介しておく。

PyFoamと呼ばれるOpenFOAMのコマンド入力を省力化してくれるツール（といっても基本はコマンド入力して使うツール）が古くからコミュニティに存在していたが、これもDEXCSランチャーやTreeFOAM上のボタンを押せば実行できるように組み込むことで、GUIプログラムそのものの開発を省力化している。

3次元CADツールとしては、blender\footnote{
\label{blender}
blender https://www.blender.org/
}、FreeCAD\footnote{
\label{FreeCAD}
FreeCAD https://www.freecadweb.org/?lang=ja
}を搭載している。前者はポリゴン系、後者はソリッド系モデルを取り扱うことの出来るCADツールとしてオープン系では代表的なものであり、通常入手できるツールに加えてDEXCSではOpenFOAMのメッシュ作成用パラメタファイルを自動作成する機能を追加してある。

いずれも簡単なモデルを作成することと、商用のCADツールで作成したモデルをインポートしてOpenFOAM用の入力ファイルに変換するツールとしての利用を想定している。

DEXCS2013までは、blenderからSwiftSnapというsnappyHexMesh作成用のパラメタファイル作成ツールを標準としていたが、DEXCS2014以降、現在はFreeCADから独自のマクロを使ってcfMesh作成用パラメタファイルを作成するようにした。

cfMeshというのは、CreativeFields社\footnote{
https://cfmesh.com/
}がリリースしているcfMeshProというOpenFOAM用のメッシュ作成ツールのうち、GUIを除くメッシュ作成エンジンだけはオープンソースとして公開しているものである。出来上がったメッシュの外観としてはOpenFOAMの標準メッシャーであるsnappyHexMeshとほぼ同等のメッシュを自動作成するツールである。
　
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{dexcs/1-12.png}
\caption{cfMeshとsnappyHexMeshの比較} 
\label{fig:cfMeshVSsnappHex}
\end{figure}

snappyHexMeshと比べて、複領域メッシュを作成できないという欠点はあるものの、パラメタ設定が容易で、特に図\ref{fig:cfMeshVSsnappHex}に示すようにレイヤー層をきちんと作成できるというメリットがあって、DEXCSでは製品版のGUI部分をFreeCADマクロで補完し、これをDEXCSランチャーでのデフォルトメッシャーとして採用している。

DEXCS2019からは、DEXCS2009以来一貫していたDEXCSランチャーにおける仮想風洞試験の内容はそのままであるが、ボタン操作のメニュー画面を一新した。

\begin{figure}[H]
\centering
\includegraphics[scale=0.45]{dexcs/1-13.png}
\caption{DEXCSランチャー} 
\label{fig:DEXCS-launcherNew}
\end{figure}

これにより、これまでDEXCSランチャーでは仮想風洞試験（simpleFoam）しか取り扱えなかったという壁があったが、この壁は無くなった。